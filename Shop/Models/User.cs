﻿
using System.ComponentModel.DataAnnotations;

namespace Shop.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "This is a mandatory field")]
        [MaxLength(20, ErrorMessage = "This field should have between 3 and 20 characters")]
        [MinLength(3, ErrorMessage = "This field should have at least 3 characters")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "This is a mandatory field")]
        [MaxLength(20, ErrorMessage = "This field should have between 3 and 20 characters")]
        [MinLength(3, ErrorMessage = "This field should have at least 3 characters")]
        public string Password { get; set; }

        public string Role { get; set; }
    }
}
