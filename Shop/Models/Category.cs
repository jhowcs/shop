﻿using System.ComponentModel.DataAnnotations;

namespace Shop.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "This is a mandatory field")]
        [MaxLength(60, ErrorMessage = "This field should have between 3 and 60 characters")]
        [MinLength(3, ErrorMessage = "This field should have at least 3 characters")]
        public string Title { get; set; }
    }
}
