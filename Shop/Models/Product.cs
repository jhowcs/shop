﻿
using System.ComponentModel.DataAnnotations;

namespace Shop.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "This is a mandatory field")]
        [MaxLength(60, ErrorMessage = "This field should have between 3 and 60 characters")]
        [MinLength(3, ErrorMessage = "This field should have at least 3 characters")]
        public string Title { get; set; }

        [MaxLength(1024, ErrorMessage = "This field might have up to 1024 characters")]
        public string Description { get; set; }

        [Required(ErrorMessage = "This is a mandatory field")]
        [Range(1, int.MaxValue, ErrorMessage = "Price should be greater than zero (0)")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "This is a mandatory field")]
        [Range(1, int.MaxValue, ErrorMessage = "Invalid Category")]
        public int CategoryId { get; set; }

        public Category Category { get; set; }
    }
}
