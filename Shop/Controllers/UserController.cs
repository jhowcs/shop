﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shop.Data;
using Shop.Models;
using Shop.Services;

namespace Shop.Controllers
{
    [Route("users")]
    public class UserController : ControllerBase
    {
        private readonly TokenService _tokenService;

        public UserController(TokenService tokenService)
        {
            _tokenService = tokenService;
        }

        [HttpGet]
        [Authorize(Roles = "manager")]
        public async Task<ActionResult<List<User>>> Get(
            [FromServices] DataContext context)
        {
            var users = await context.Users.AsNoTracking().ToListAsync();
            return users;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<User>> Post(
            [FromServices] DataContext context,
            [FromBody] User model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userCount = await context
                        .Users
                        .AsNoTracking()
                        .CountAsync(u => u.UserName == model.UserName);
                   
                    if (userCount > 0)
                    {
                        return BadRequest(new { message = "This username is already in use!" });
                    } else
                    {
                        context.Users.Add(model);
                        await context.SaveChangesAsync();
                        return Created($"users/{model.Id}",
                            new
                            {
                                model.UserName,
                                model.Role
                            });                        
                    }
                }
                catch (Exception)
                {
                    return BadRequest(new { message = "Problem creating the user!" });
                } 
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate(
            [FromServices] DataContext context,
            [FromBody] User model)
        {
            var user = await context
                .Users
                .AsNoTracking()
                .Where(u => u.UserName == model.UserName && u.Password == model.Password)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                return NotFound(new { message = "User name or password invalid!" });
            }

            var token = _tokenService.GenerateToken(user);

            return Ok(new
            {
                user = user.UserName,
                token = token
            });
        }
    } 
}
