﻿namespace Shop.Services
{
    public class JwtTokenConfig
    {
        public string Secret { get; set; }
        public int AccessTokenExpiration { get; set; }
    }
}
